﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f;
    public Rigidbody2D rb;
    public Camera cam;

    Vector2 movement;
    Vector2 mousePos;
    // Update is called once per frame
    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        if (rb.position.x < -6)
        {
            movement.x = 0.1f;
        }

        if (rb.position.y < -6)
        {
            movement.y = 0.1f;
        } else if(rb.position.y > 6)
        {
            movement.y = -0.1f;
        }

        //move space ship
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

        //rotate space ship
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        Vector2 lookDir = mousePos - rb.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = angle;
    }

    void OnCollisionEnter2D(Collision2D collision) {

        if (collision.gameObject.tag == "Obstacle")
            SceneManager.LoadScene(0);
    }
}
